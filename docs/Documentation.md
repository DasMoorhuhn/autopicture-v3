# HelperClass

## ExifData

Diese Klasse dient als Model für die Parameter eines Bildes. Für jedes Bild wird ein Objekt angelegt.

## ScanDir(self, logger:logging)

In dieser Klasse befinden sich die Methoden zum Scannen des temporären Bilder Pfades.

### recursiveScanDir(self, pathForScan:str)

Hiermit wird der im Parameter übergebene Ordner rekursiv nach Dateien gescannt. Die Methode gibt eine liste mit allen Bildern in diesem Verzeichnis zurück

### scanFolder

### countImages

Diese Methode gibt die Anzahl der gescannten Bilder zurück.

## getMeta()

### getMetaData(self, images:list)


