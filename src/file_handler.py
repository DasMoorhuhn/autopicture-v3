import os
import sys
sys.path.append("../")
import shutil
import logging
from progressbar.progressbar import ProgressBar

from src.exif_data import ExifData


def sort_pictures(images:list, dst:str, logger:logging.Logger):
  image_total = len(images)
  image_counter = 0
  progress_bar = ProgressBar(
    maxval=image_total,
    term_width=70
  )

  progress_bar.start()

  for image in images:
    image:ExifData
    if not image: continue
    path = os.path.join(dst, str(image.make), str(image.year), str(image.month), str(image.day))
    if not os.path.exists(path): os.makedirs(path)

    stat_info = os.stat(image.path)
    image_dst = f"{path}{os.sep}{image.name}"
    shutil.move(src=image.path, dst=image_dst)
    # os.chmod(path=f"{path}/{image.name}", mode=stat_info.st_mode)
    logger.info(f"Moved {image.path} -> {image_dst}")

    progress_bar.update(image_counter)
    image_counter += 1
  progress_bar.finish()


def sort_raws(raws:list, dst:str, logger:logging):
  pass

