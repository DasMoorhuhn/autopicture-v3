import filetype


class MimeTypes:
  def __init__(self, file_path):
    self.is_image = False
    self.is_video = False
    self.is_raw = False
    self.is_unsupported_file_type = False
    self.__proceed(file_path)

  def __proceed(self, file_path):
    if filetype.is_image(file_path):
      self.is_image = True
    elif filetype.is_video(file_path):
      self.is_video = True
    else:
      self.is_unsupported_file_type = True
