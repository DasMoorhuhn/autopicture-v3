import sys
import logging
from process import start_process

sys.path.append("../")
log_folder = "."

logger = logging.getLogger('AutoPicture')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename=f'{log_folder}/AutoPicture.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s|:%(message)s'))
logger.addHandler(handler)

start_process(logger=logger)
