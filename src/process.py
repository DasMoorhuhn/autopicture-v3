import os
import sys
import i18n
sys.path.append("../")

from time import time
from src.meta_data_handler import get_meta_data
from src.file_handler import sort_pictures
from src.scan_folder import recursive_scan_folder
from src.config import get_config

workdir, filename = os.path.split(os.path.abspath(__file__))
config = get_config()

i18n.set('locale', config.language)
i18n.set('fallback', 'en')
i18n.set('filename_format', '{locale}.{format}')
i18n.load_path.append(f'{workdir}{os.sep}i18n_translations{os.sep}')


def start_process(logger):
  try:
    files = recursive_scan_folder(config.src)
    if len(files) > 0:
      start_timer = time()
      exif_data = get_meta_data(images=files)
      image_total = len(exif_data)
      print(i18n.t('start_sorting_images', image_count=image_total))

      sort_pictures(images=exif_data, dst=config.dst, logger=logger)
      end_timer = time()
      duration = round(end_timer - start_timer, 2)

      print(i18n.t('done'))
      print(i18n.t('done_sorting_images', time=duration, image_count=image_total))
      return True
    else:
      print(i18n.t('no_images_found'))
      return False
  except Exception as err:
    print(err)
    logger.error(err)
    raise err
