import yaml
import os

workdir, filename = os.path.split(os.path.abspath(__file__))
config_file = f"{workdir}{os.sep}config.yml"


def get_config():
  with open(file=config_file, mode='r') as file:
    return Config(yaml.safe_load(file))


class Config:
  def __init__(self, data):
    self.src = data['src']
    self.dst = data['dst']
    self.language = data['language']
