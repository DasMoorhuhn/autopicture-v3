import magic
import sys
import os
import filetype
from PIL import Image
from PIL import ExifTags

sys.path.append("../")
from src.exif_data import ExifData
from src.mime_types import MimeTypes

video_formats = ["MP4", "MOV", "M4V", "MKV", "AVI", "WMV", "AVCHD", "WEBM", "MPEG"]
picture_formats = ["JPG", "JPEG", "PNG"]
raw_formats = ["CR2", "RAF", "RW2", "ERF", "NRW", "NEF", "ARW", "RWZ", "EIP",
               "DNG", "BAY", "DCR", "GPR", "RAW", "CRW", "3FR", "SR2", "K25",
               "KC2", "MEF", "DNG", "CS1", "ORF", "MOS", "KDC", "CR3", "ARI",
               "SRF", "SRW", "J6I", "FFF", "MRW", "MFW", "RWL", "X3F", "PEF",
               "IIQ", "CXI", "NKSC", "MDC"]

key_words = ["DateTime", "Make"]


def is_file_video(path:str):
  mime = magic.Magic(mime=True)
  file = mime.from_file(path)
  if file.find('video') != -1: return True
  else: return False


def is_file_picture(path:str):
  mime = magic.Magic(mime=True)
  file = mime.from_file(path)
  if file.find('picture') != -1: return True
  else: return False


def handle_raw(image:str):
  image_creation_time = os.path.getctime(filename=image)
  print(image_creation_time)


def handle_image(image:str):
  img = Image.open(image)
  values = []
  for tag, text in img.getexif().items():
    if tag in ExifTags.TAGS: values.append([ExifTags.TAGS[tag], str(text)])
  return filter_date_and_make(meta_tags=filter_data(values=values), image_path=image)


def handle_video(video:str):
  pass


def get_image_meta_data(image_path):
  image_extension = str(image_path).split("/")[-1].split(".")[1].upper()
  # TODO: Sort out videos using mime type of file
  # mime = MimeTypes(file_path=image_path)

  if image_extension in picture_formats: return handle_image(image=image_path)
  elif image_extension in video_formats: return handle_video(video=image_path)
  elif image_extension in raw_formats: return handle_raw(image=image_path)


def filter_date_and_make(meta_tags:list, image_path):
  make = meta_tags[0][1]
  image_name = str(image_path).split("/")[-1]

  date_time = meta_tags[1][1].split(" ")
  date, time = date_time[0].split(":"), date_time[1]
  year, month, day = date[0], date[1], date[2]

  exif_data_dict = {
    "day": day,
    "month": month,
    "year": year,
    "make": make,
    "time": time,
    "image_path": image_path,
    "image_name": image_name
  }

  return ExifData(exif_data_dict)


def filter_data(values):
  """Filters the data according to the meta tags from the keyword list"""
  value_return = []
  for value in values:
    value_return.append(value) if value[0] in key_words else {}
  return value_return


def get_meta_data(images: list):
  exif_data_list = []
  for image in images:
    exif_data_list.append(get_image_meta_data(image_path=image))
  return exif_data_list
