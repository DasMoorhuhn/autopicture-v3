import requests
import json

# Proof of concept


class Version:
  def __init__(self, data:dict):
    self.version:str = data['version']
    self.version_int:int = int(self.version.replace(".", ""))
    self.date:str = data['date']


class Release:
  def __init__(self, data:dict):
    self.name = data['name']
    self.tag_name = data['tag_name']
    self.description = data['description']
    self.created_at = data['created_at']
    self.released_at = data['released_at']
    self.upcoming_release = data['upcoming_release']
    self.version_int = int(self.tag_name.replace(".", ""))
    self.zip_file_url = data['assets']['sources']
    self.__proceed()

  def __proceed(self):
    for assest in self.zip_file_url:
      if assest['format'] == 'zip':
        self.zip_file_url = assest['url']
        break


def read_version():
  with open(file=".version.json") as file:
    version = Version(json.load(file))
  return version


def install():
  pass


def check_for_update():
  version_current = read_version()
  request = "https://gitlab.com/api/v4/projects/52637155/releases"
  response = requests.get(url=request, timeout=1)
  if not response.ok: return

  # Get the latest release
  releases_json = json.loads(response.text)
  # index version
  latest_release = [0, 0]
  for release_json in releases_json:
    release = Release(release_json)
    if release.version_int > version_current.version_int:
      latest_release[0] = releases_json.index(release_json)
      latest_release[1] = release.version_int

  if latest_release == [0, 0]: return
  release = Release(releases_json[latest_release[0]])
  print(f"v{version_current.version} -> v{release.tag_name}")

  if release.version_int > version_current.version_int:
    # Update
    print("Update")
    print(release.zip_file_url)


check_for_update()
