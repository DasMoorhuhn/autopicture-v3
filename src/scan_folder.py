import os


def scan_folder(path:str):
  return next(os.walk(path))[2]


def recursive_scan_folder(path:str):
  results = []
  for root, folders, files in os.walk(path):
    list_files = os.listdir(root)
    for file in list_files:
      current_file = f"{root}{os.sep}{file}"
      if os.path.isfile(current_file): results.append(current_file)
  return results
