class ExifData:
  """This is for an object that stores the data of a picture"""
  def __init__(self, data:dict) -> None:
    self.path:str = str(data['image_path'])
    self.name:str = str(data['image_name'])
    self.day:int = int(data['day'])
    self.month:int = int(data['month'])
    self.year:int = int(data['year'])
    self.time = data['time']
    self.make:str = str(data['make'])
