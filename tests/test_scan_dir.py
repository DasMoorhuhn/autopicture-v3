import unittest

from helpers.folder_helper import delete_folder
from helpers.folder_helper import create_folders
from helpers.folder_helper import create_file
from helpers.folder_helper import TEST_FOLDER

from src.scan_folder import scan_folder
from src.scan_folder import recursive_scan_folder


class TestScanFolder(unittest.TestCase):
  def test_scan_folder(self):
    create_folders()
    create_file(file='img_01.jpeg')
    files = scan_folder(TEST_FOLDER)
    delete_folder()

    assert len(files) == 1

  def test_scan_empty_folder(self):
    create_folders()
    files = scan_folder(TEST_FOLDER)
    delete_folder()

    assert len(files) == 0

  def test_scan_recursive_folder(self):
    create_folders()
    create_file(file='img_01.jpeg')
    create_file(file='img_02.jpeg')
    create_file(file='001/img_03.jpeg')
    create_file(file='001/001/img_04.jpeg')
    create_file(file='002/001/img_05.jpeg')

    files = recursive_scan_folder(TEST_FOLDER)
    delete_folder()

    assert len(files) == 5

  def test_scan_recursive_empty_folder(self):
    create_folders()
    files = recursive_scan_folder(TEST_FOLDER)
    delete_folder()

    assert len(files) == 0
