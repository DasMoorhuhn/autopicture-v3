with open(file="coverage.xml") as file:
  file_content = file.readlines()


for line in file_content:
  try:
    orig_line = file_content.index(line)
    line = line.strip().split("<")[1].split(">")
    if line[0] == "source":
      print("FOUND")
      file_content[orig_line] = "<source>src</source>\n"
  except:
    pass


with open(file="coverage.xml", mode="w") as file:
  file.writelines(file_content)
