import unittest

from src import config as config_module


class TestConfig(unittest.TestCase):
  def test_config_class(self):
    test_config = {
      "src": "/src/src",
      "dst": "/src/dst",
      "language": "en"
    }

    config = config_module.Config(test_config)
    assert config.src == "/src/src"
    assert config.dst == "/src/dst"
    assert config.language == "en"

  def test_get_config(self):
    config_module.config_file = "src/config.yml"
    config = config_module.get_config()
    assert config.src == "../app/Temp"
    assert config.dst == "../app/Bilder"
    assert config.language == "en"
