import unittest

from src.mime_types import MimeTypes


class TestMimeTypes(unittest.TestCase):
  def test_mime_type_image(self):
    mime_type = MimeTypes(file_path="tests/test_files/test_image_001.JPG")
    assert mime_type.is_image
    assert not mime_type.is_video
    assert not mime_type.is_unsupported_file_type

  def test_mime_type_video(self):
    mime_type = MimeTypes(file_path="tests/test_files/test_video.mp4")
    assert not mime_type.is_image
    assert mime_type.is_video
    assert not mime_type.is_unsupported_file_type

  def test_mime_type_unsupported_file_type(self):
    mime_type = MimeTypes(file_path="tests/test_mime_types.py")
    assert not mime_type.is_image
    assert not mime_type.is_video
    assert mime_type.is_unsupported_file_type

