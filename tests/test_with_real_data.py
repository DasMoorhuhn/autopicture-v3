import unittest

from helpers.folder_helper import TEST_FOLDER
from helpers.folder_helper import copy_images

from src.meta_data_handler import get_meta_data
from src.scan_folder import recursive_scan_folder


class TestSamsung(unittest.TestCase):
  def test_a54(self):
    copy_images(brand="samsung", model="a54")
    files = recursive_scan_folder(path=TEST_FOLDER)
    meta_data = get_meta_data(images=files)
    for image in meta_data:
      assert image.make == "samsung"

    image = next((image for image in meta_data if image.name == "samsung_a54_001.jpg"), None)
    assert image.day == 2
    assert image.month == 12
    assert image.year == 2023

    image = next((image for image in meta_data if image.name == "samsung_a54_003.jpg"), None)
    assert image.day == 8
    assert image.month == 12
    assert image.year == 2023

  @unittest.skip("")
  def test_a52s(self):
    copy_images(brand="samsung", model="a52s")
    files = recursive_scan_folder(path=TEST_FOLDER)
    meta_data = get_meta_data(images=files)
    for image in meta_data:
      assert image.make == "samsung"

  @unittest.skip("")
  def test_a14(self):
    copy_images(brand="samsung", model="a14")
    files = recursive_scan_folder(path=TEST_FOLDER)
    meta_data = get_meta_data(images=files)
    for image in meta_data:
      assert image.make == "samsung"


class TestApple(unittest.TestCase):
  def test_iphone_x(self):
    copy_images(brand="iphone", model="x")
    files = recursive_scan_folder(path=TEST_FOLDER)
    meta_data = get_meta_data(images=files)
    for image in meta_data:
      assert image.make == "Apple"

