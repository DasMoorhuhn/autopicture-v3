from src.exif_data import ExifData
import unittest


class TestExifData(unittest.TestCase):
  def test_exif_data(self):
    exif_data_dict = {
      "day": 2,
      "month": 2,
      "year": 2222,
      "make": "CAMERA",
      "time": "10:10:10",
      "image_path": "/path/to/image",
      "image_name": "Image.jpeg"
    }
    exif_data = ExifData(exif_data_dict)

    assert exif_data.make == "CAMERA"
    assert exif_data.year == 2222
    assert exif_data.time == "10:10:10"

