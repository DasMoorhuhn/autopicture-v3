apk add --update libmagic
pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 -m pip install -r develop_requirements.txt
pwd
sh tests/start_tests.sh
exit_code=$?

cp tests/coverage/coverage.xml ./coverage.xml
cp tests/coverage/report.xml ./report.xml

python3 tests/get_coverage_percent.py
exit $exit_code