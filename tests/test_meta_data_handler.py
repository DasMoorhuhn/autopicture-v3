import unittest
import os

from src.meta_data_handler import get_meta_data
from src.meta_data_handler import get_image_meta_data
from src.meta_data_handler import filter_data
from src.meta_data_handler import filter_date_and_make
from src.meta_data_handler import is_file_video
from src.scan_folder import recursive_scan_folder

TEST_IMAGES = "tests/test_files"


class TestMetadataHandler(unittest.TestCase):
  def test_get_image_meta_data(self):
    image_exif_data = get_image_meta_data(image_path=f"{TEST_IMAGES}/test_image_001.JPG")
    assert image_exif_data.name == "test_image_001.JPG"

  def test_get_meta_data(self):
    images = recursive_scan_folder(TEST_IMAGES)
    print(images)
    exif_data = get_meta_data(images)
    assert len(exif_data) == len(images)
