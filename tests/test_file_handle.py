import unittest
from unittest.mock import Mock

from helpers.folder_helper import TEST_FOLDER
from helpers.folder_helper import TEST_IMAGE_FOLDER
from helpers.folder_helper import delete_folder
from helpers.folder_helper import copy_test_images

from src.file_handler import sort_pictures
from src.scan_folder import recursive_scan_folder
from src.meta_data_handler import get_meta_data


class TestFileHandler(unittest.TestCase):
  def test_file_handler(self):
    copy_test_images()
    files = recursive_scan_folder(TEST_FOLDER)
    exif_data = get_meta_data(files)
    sort_pictures(images=exif_data, logger=Mock(), dst=TEST_IMAGE_FOLDER)
    sorted_pictures = recursive_scan_folder(TEST_FOLDER)
    delete_folder()

    assert len(sorted_pictures) == 2
    assert "".join(picture for picture in sorted_pictures if '2023/10/25/test_image_002.JPG' in picture) != ""
    assert "".join(picture for picture in sorted_pictures if '2023/10/28/test_image_001.JPG' in picture) != ""
