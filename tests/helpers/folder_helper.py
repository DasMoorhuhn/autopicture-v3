import os
import shutil
from pathlib import Path

from src.scan_folder import recursive_scan_folder

TEST_FOLDER = ".test_folder"
TEST_IMAGES = f"tests{os.sep}test_files"
TEST_TEMP_FOLDER = os.path.join(TEST_FOLDER, 'Temp')
TEST_IMAGE_FOLDER = os.path.join(TEST_FOLDER, 'Images')


def create_file(file):
  Path(os.path.join(TEST_FOLDER, file)).touch()


def delete_folder():
  shutil.rmtree(TEST_FOLDER, ignore_errors=True)


def create_folders():
  delete_folder()
  os.makedirs(os.path.join(TEST_FOLDER, '001', '001'))
  os.makedirs(os.path.join(TEST_FOLDER, '002', '001'))


def copy_test_images():
  delete_folder()
  os.makedirs(TEST_TEMP_FOLDER)
  os.makedirs(TEST_IMAGE_FOLDER)
  shutil.copy(src=os.path.join(TEST_IMAGES, 'test_image_001.JPG'), dst=TEST_TEMP_FOLDER)
  shutil.copy(src=os.path.join(TEST_IMAGES, 'test_image_002.JPG'), dst=TEST_TEMP_FOLDER)


def copy_images(brand:str, model:str):
  delete_folder()
  create_folders()
  files = recursive_scan_folder(path=TEST_IMAGES)
  for file in files:
    file_name = file.split(os.sep)[2:][0]
    file_name = file_name.split("_")
    if file_name[0] == brand and file_name[1] == model:
      shutil.copy(src=file, dst=TEST_FOLDER)

