# AutoPicture V3

Bildersortierprogramm geschrieben in Python3.12.

Example structure:
```bash
app/Bilder/
└── SONY
    └── 2023
        ├── 10
        │   ├── 21
        │   │   ├── DSC02975.JPG
        │   │   └── DSC02976.JPG
        │   ├── 25
        │   │   ├── DSC03030.JPG
        │   │   ├── DSC03031.JPG
        │   │   └── DSC03096.JPG
        │   ├── 28
        │   │   ├── DSC03097.JPG
        │   │   ├── DSC03098.JPG
        │   │   └── DSC03116.JPG
        │   ├── 29
        │   │   ├── DSC03117.JPG
        │   │   ├── DSC03118.JPG
        │   │   └── DSC03135.JPG
        │   └── 30
        │       ├── DSC03136.JPG
        │       └── DSC03137.JPG
        ├── 11
        │   ├── 16
        │   │   └── DSC03144.JPG
        │   ├── 17
        │   │   ├── DSC03145.JPG
        │   │   └── DSC03146.JPG
        │   └── 28
        │       ├── DSC03153.JPG
        │       ├── DSC03154.JPG
        │       └── DSC03155.JPG
        ├── ...


```

# Erste Schritte

## Python

### Linux

Debian:
```bash
sudo apt-get install -y python3 python3-pip
```

### Windows

TODO: 
Download the latest version of Python and install it

## Pip

```bash
pip3 install -r requirements.txt
```

## Config

## Starten

# Tests

```bash
sh ./tests/start_tests.sh
```

