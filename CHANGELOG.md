## v0.2.0 []
- Added i18n for multi language support
- Fixed tests
- Fixed gitlab CI file
- Fixed bug on string splitting at filtering date/time
- Only pictures will be sorted
- WIP: RAW sorting
## v0.1.0 [2023-11-30]
- Added updater
- Added Changelog file
- Changed version file from yaml to json
